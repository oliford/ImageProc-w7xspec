package imageProc.w7xspec;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.math3.util.FastMath;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import de.mpg.ipp.codac.w7xtime.NanoTime;
import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBJSONInfoDesc.ExtraJsonRequestArg;
import imageProc.w7xspec.LightPaths.Component;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import w7x.archive.ArchiveDBFetcher;

/** Access routines for spectrometer calibration, drawing data from ArchiveDB
 * 
 * Equivalent to python/W7XSpec/w7xspec/specCal.py
 *  */
public class SpecCal {
	
		public double commonWavelengthOffset = 0; 
	
		public static class ChanFit {
			public double x0;
			public double[] coeffs;
		}
	
		/** Channel fit data: chanFits.get(channelName).get(propertyName) */
		public HashMap<String, HashMap<String, ChanFit>> chanFits = new HashMap<String, HashMap<String, ChanFit>>();
	
		public SpecCal(String system, String component, long time){
			this(system, component, time, null, null, false, 0);
		}
	
	    /** Create SpecCal object for given spectrometer, pointed at given target wavelength.

	       @param system - diagnostic system name, e.g. QSK_CXRS
	       @param component - Spectrometer name, e.g. ILS_Green
	       @param target - Spectrometer target/settings ID, e.g. C_VI_529, 
	       @param time - Nanosecond timestamp of data for which calibration is needed.
	    */
		public SpecCal(String system, String component, long time, String path, String target, boolean noIntensity){
			this(system, component, time, path, target, noIntensity, 0);
			 
		}
		 
	    public SpecCal(String system, String component, long time, String path, String target, boolean noIntensity,
	    		double wavelengthOffset){
	    	Logger logr = Logger.getLogger(SpecCal.class.getName());
	        this.commonWavelengthOffset = wavelengthOffset;
	    	
	        if(target == null || target.length() == 0){
	            target = Defaults.getTarget(system, component);
	        }

	        if(path == null){ 
	            path = Defaults.getDatastreamPath(system, component);
	        }
	        
	        if(target == null) {
	        	ArchiveDBDesc desc = new ArchiveDBDesc(path);
	        	desc.setNanosRange(time, time);
	        	target = Metadata.getMetadataFromNotes(desc, "TARGET");
        		logr.fine("Got target=" + target + " from notes");
	        }

            if(target == null)
                throw new RuntimeException("No default target known for component '"+component+"'. Must be specified to get calibration data.");
            
            if(commonWavelengthOffset == 0) {
	        	ArchiveDBDesc desc = new ArchiveDBDesc(path);
	        	desc.setNanosRange(time, time);
            	String str = Metadata.getMetadataFromNotes(desc, "SpecCal/config/wavelnCommonOffset");
            	if(str != null) {
            		commonWavelengthOffset = Algorithms.mustParseDouble(str);
            		logr.fine("Got commonWavelengthOffset=" + commonWavelengthOffset + " from notes");
            	}
            	
            }	

	        loadCalibrationFits(system, component, target, "Wavelength", time);

	        if(!noIntensity) {
	            loadCalibrationFits(system, component, target, "Intensity", time);
	            

				//changed the slit width at start of October 2018 and calibration was
	            //done later with the changed width. So everything before was lower 
				if(system.equals("QSK_CXRS") && component.equals("AUG2") 
						&& time <  ArchiveDBDesc.toNanos(ZonedDateTime.of(2018, 10, 4, 0, 0, 0, 0, ZoneId.of("UTC")))) {

					for(HashMap<String, ChanFit> chanEntry : chanFits.values()) {
						ChanFit intensity = chanEntry.get("Intensity");
						if(intensity != null) {
							intensity.coeffs[0] /= 2.2;
						}
					}
					
				}
	        }
	    }
	        
	    /**
	       Extract row polynominal coefficients from archive

	       @param system - diagnostic system name, e.g. 'QSK', or 'QSK_CXRS'
	       @param component - Spectrometer name, e.g. ILS_Green
	       @param target : Target spectrometer configuration of calibration
	       @param set : Calibration set. Makes up path. e.g. 'Wavelength' or 'Intensity'
	       @param time : Time of data for which calibration is required
	    */
	    private void loadCalibrationFits(String system, String component, String target, String set, long time){
	        Logger logr = Logger.getLogger(SpecCal.class.getName());
	        
	        String table = system.replaceAll("_.*", ""); 

	        Component c = LightPaths.getComponent(table, component, time);
	        
	        String path  = c.wavelengthCalibrationPath;
	        
	        if(path == null)        
	        	path = Defaults.getCalibrationPath(system, component, set, target);
	        		
	        ArchiveDBDesc desc = new ArchiveDBDesc("w7x/" + path);
	        //desc.setDatabase(Defaults.getDatabase(system));
	        //desc.setSection(Section.raw);
	        //desc.setProject("W7XAnalysis");
	        //desc.setSignalGroup(Defaults.getFullSystemName(system));
	        //desc.setSignalName("Calibration_" + set + "_" + component + "_" + target);
	        //desc.setType(Type.PARLOG);
	        desc.setNanosRange(time, time);
	        desc.setVersion(0);

	        System.err.println(desc.getWebAPIRequestURI());

	        Gson gson = new Gson();
	   
            //raise RuntimeError("No version (no data) for path '%s' at %s" % (path, archivedb.utils.to_stringdate(time)));
	        logr.fine("Loading wavelength + instrument function data from " + desc);
	        
	        int version = ArchiveDBFetcher.defaultInstance().getHighestVersionNumber(desc);
	        desc.setVersion(version);
	        
	        JsonObject parlog = ArchiveDBFetcher.defaultInstance().getParlogJson(desc, ExtraJsonRequestArg.MAP_TO_ARRAY);
	        if(parlog.get("label").getAsString().equals("EmptySignal"))
	        	throw new RuntimeException("No parlog for calibration at " + desc);
	        
	        logr.fine("Calibration data validity " 
    				+ NanoTime.toUTCString(parlog.get("dimensions").getAsJsonArray().get(0).getAsLong())
    				+ NanoTime.toUTCString(parlog.get("dimensions").getAsJsonArray().get(1).getAsLong()) );
	        
	        JsonObject p = parlog.get("values").getAsJsonArray().get(0).getAsJsonObject();
	        if(target.equalsIgnoreCase("good")) {
	        	if(component.equals("NIFS_C")){
		        	p = (JsonObject)p.get("grating_C_VI_529");
		        	p = (JsonObject)p.get("slit_-800");
		        	p = (JsonObject)p.get("iris_25000");
	        	}else if(component.equals("PassiveCarbon")){
		        	p = (JsonObject)p.get("grating_");
		        	p = (JsonObject)p.get("slit_");
	        	}else{
	        		throw new RuntimeException("Need to implement fetching the current grating/slit/iris etc for each spectrometer");
	        	}
	        }
	        JsonObject chanFitsJson = p.get("chanFits").getAsJsonObject();
	        
	        for(String origChanName : chanFitsJson.keySet()){
	        	JsonObject chanFitJson= chanFitsJson.get(origChanName).getAsJsonObject();

				String newChanName = Defaults.fixROINames(system, component, new String[] { origChanName })[0];
	        	HashMap<String, ChanFit> chanFit = chanFits.get(newChanName);
	        	if(chanFit == null){
	        		chanFit = new HashMap<String, ChanFit>();
	        	}
	        	
	        	for(String propName : chanFitJson.keySet()){
	        		JsonElement propJson = chanFitJson.get(propName);
	        		ChanFit cf = gson.fromJson(propJson, ChanFit.class);
	        		
	        		chanFit.put(propName, cf);
	        	}
	        	
	        		        	
        		chanFits.put(newChanName, chanFit);	        	
	        }
	        
	    }

	    /**
	        Gets the wavelength vector for a given spectrometer channel.

	        params :
	            @param chanName : Channel/fibre name, e.g. "ILS:04"
	            @param pixel0 : Sensor pixel number of first sensor pixel in the first image pixel (get from w7xspec.metadata.getTrackROIOffset()).
	            @param nWavelengths : Number of wavelengths to output (i.e. number of pixels in image data)

	    */
	    public double[] getWavelengthVector(String chanName, int pixel0, int nWavelengths){
	        return getCalibrationVector(chanName, "lambda", pixel0, nWavelengths, 1);
	    }

	    /**
	        Gets the wavelength vector for a given spectrometer channel.

	        params :
	            @param chanName : Channel/fibre name, e.g. "ILS:04"
	            @param pixel0 : Sensor pixel number of first sensor pixel in the first image pixel (get from w7xspec.metadata.getTrackROIOffset()).
	            @param nWavelengths : Number of wavelengths to output (i.e. number of pixels in image data)
	            @param pixelBinning : Number of sensor pixels per image pixel (get from w7xspec.metadata). Usually 1 (default)

	    */
	    public double[] getWavelengthVector(String chanName, int pixel0, int nWavelengths, int pixelBinning){
	        double[] l =  getCalibrationVector(chanName, "lambda", pixel0, nWavelengths, pixelBinning);
	        if(commonWavelengthOffset != 0)
	        	for(int i=0; i < l.length; i++)
	        		l[i] += commonWavelengthOffset;	        	
	        return l;
	    }
	    
	    /**
	        Gets the Intensity calibration vector for a given spectrometer channel [photoelectrons photon^-1 m^2 SR nm]

	        params :
	            @param chanName : Channel/fibre name, e.g. "ILS:04"
	            @param pixel0 : Sensor pixel number of first sensor pixel in the first image pixel (get from w7xspec.metadata.getTrackROIOffset()).
	            @param nWavelengths : Number of wavelengths to output (i.e. number of pixels in image data)
	            @param pixelBinning : Number of sensor pixels per image pixel (get from w7xspec.metadata). Usually 1 (default)

	    */
	    public double[] getIntensityCalibrationVector(String chanName, int pixel0, int nWavelengths, int pixelBinning){
	        return getCalibrationVector(chanName, "Intensity", pixel0, nWavelengths, pixelBinning);
	    }

	    /** 
	        Gets the Instrument function width and power vector for a given spectrometer channel.
	          This gives a description of the instrument function variation across the chip (i.e. wavelength)
	          for a given channel/track/row.

	          The instrument function is a 'supergaussian':  ~ exp( -((x - x0)/s)^p / 2 );
	            where p is the power (2 for a standard Gaussian)
	              and s is the width/standard deviation (i.e. 'sigma')

	        
	        @param chanName : Channel/fibre name, e.g. "ILS:04"
			@param pixel0 : Sensor pixel number of first sensor pixel in the first image pixel (get from w7xspec.metadata.getTrackROIOffset()).
	        @param nWavelengths : Number of wavelengths to output (i.e. number of pixels in image data)
	        @param pixelBinning : Number of sensor pixels per image pixel (get from w7xspec.metadata). Usually 1
	        @return double[][]{ sigma[x], power[x] }

	    */
	    public double[][] getInstrumentFunctionWidthVector(String chanName, int pixel0, int nWavelengths, int pixelBinning){
	    	
	        double[] s = getCalibrationVector(chanName, "Sigma", pixel0, nWavelengths, pixelBinning);
	        
	        //Also the supergaussian 'power'. 
	        double p[];
	        if(chanFits.containsKey(chanName) && chanFits.get(chanName).containsKey("Power")){	        
	            p = getCalibrationVector(chanName, "Power", pixel0, nWavelengths, pixelBinning);
	            		
	        }else if(s == null){
	            p = null;
	            
	        }else{  //Assume 2 for a standard gaussian if 'Sigma' is in the calibration set but 'Power' isn't
	            p = Mat.fillArray(2.0, nWavelengths);
	        }

	        return new double[][]{ s, p };
	    }
	    
	    public double[] getCalibrationVector(String chanName, String prop, int pixel0, int nWavelengths, int pixelBinning){
	    	Logger logr = Logger.getLogger(SpecCal.class.getName());
	    	
	    	//hack for ILS and NIFS, that we switched to using the acqqusition device name, rather than
	    	//the head set, i.e. 'ILS_Green' instead of 'ILS'
	    	String chanNameMod = chanName.replaceAll("ILS_[RGBreendlu]*", "ILS").replaceAll("NIFS_[He]*", "NIFS");
	    	if(!chanFits.containsKey(chanName) && chanFits.containsKey(chanNameMod))
	    		chanName = chanNameMod;
	    	
	        if(!chanFits.containsKey(chanName) || !chanFits.get(chanName).containsKey(prop)){
	            logr.warning("No calibration fit of '"+prop+"' channel '"+chanName+"'.");
	            return null;
	        }

	        ChanFit s = chanFits.get(chanName).get(prop);
	        
	        double ret[] = new double[nWavelengths];
	        for(int i=0; i < nWavelengths; i++){
	        	double x = pixel0 + i * pixelBinning;
	        	
		        for(int j=0; j < s.coeffs.length; j++){
		            ret[i] += s.coeffs[j] * FastMath.pow(x - s.x0, j);
	        	}
	        }

	        return ret;
	    }

	    /**
	        Gets the calibration polynomial coefficients for a given calibration 
	        property for a given spectrometer channel.

	        These coefficients describe the variation of wavelength 
	         with _sensor_ pixel number.

	       params : 
	            @param chanName : Channel/fibre name, e.g. "ILS:04"
	            @param prop : Property from the calibration sets, 
	               e.g. one of { 'lambda', 'Intensity', 'Sigma', 'Power' }

	       returns : (x0, c)
	           wavelength polynomial coefficients [ c0, c1, c2, ... ]
	           wavelength = sum_i( c_i * (x - x0)**i ) 
	    */
	    public ChanFit getCalibration(String chanName, String prop){
	    	Logger logr = Logger.getLogger(SpecCal.class.getName());

	    	//hack for ILS and NIFS, that we switched to using the acqqusition device name, rather than
	    	//the head set, i.e. 'ILS_Green' instead of 'ILS'
	    	String chanNameMod = chanName.replaceAll("ILS_[RGBreendlu]*", "ILS")
	    									.replaceAll("NIFS_[He]*", "NIFS");
	    	
	    	if(!chanFits.containsKey(chanName) && chanFits.containsKey(chanNameMod))
	    		chanName = chanNameMod;
	    	
	        if(!chanFits.containsKey(chanName) || !chanFits.get(chanName).containsKey(prop)){
	            logr.warning("No calibration fit of '"+prop+"' channel '"+chanName+"'.");
	            return null;
	        }

	        ChanFit s = chanFits.get(chanName).get(prop);
	        
	        return s;

	    }
	    
	    public static void main(String[] args) {
	    	ConsoleHandler ch = new ConsoleHandler();
			ch.setLevel(Level.ALL);
			Logger logr = Logger.getLogger(SpecCal.class.getName());
			logr.addHandler(ch);
			logr.setLevel(Level.ALL);
			logr.fine("FOAD");
	    	
			SpecCal sp = new SpecCal("QSK_CXRS", "ILS_Red", ArchiveDBFetcher.defaultInstance().getT1("20181009.034.002"));
			double l[] = sp.getWavelengthVector("ILS:21", 0, 1000);
			Mat.dumpArray(l);
		}
}
