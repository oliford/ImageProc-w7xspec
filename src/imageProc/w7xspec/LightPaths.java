package imageProc.w7xspec;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonStreamParser;

import de.mpg.ipp.codac.w7xtime.NanoTime;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.w7x.ArchiveDB.Database;
import w7x.archive.ArchiveDBFetcher;

/**
Routines for interrogating and managing the LightPaths database of fibre connections for spectroscopy diagnostics
Uses Phython webservice from W7XSpec/lightPathsWebapp

Example 1: Load components and connections and find what is at the end of a connection path:
   import w7xspec.lightPaths.LightPaths
   lp = w7xspec.lightPaths.LightPaths("QSK_CXRS", archivedb.get_program_t0("20181009.034");

   final, path = lp.follow("ILS:21")

Result:
   final = "AEA21_A:24"
   path = "ILS[Panel]:21 = [Lab]XFER-AEA21[TH]:22 = [Port]AEA21_A:24"


Example 1: Load components and connections and find what is at the end of a connection path:
   table = "QSK";
   t = archivedb.utils.to_timestamp("2018-10-09 11:00:00");
   lp = w7xspec.lightPaths.LightPaths(table, t)

   final, path = lp.getEndComponent("ILS", None, "21")

Result:
   final = "AEA21_A:24"
   path = "ILS[Panel]:21 = [Lab]XFER-AEA21[TH]:22 = [Port]AEA21_A:24"

*/
public class LightPaths {
	
	private static String webapiAddress = "http://" + Defaults.getWebservicesHost() + ":5055";
	
	public static class Component {
		public String name;
		public String kks;
		public String losDefsPath;
		public String installed;
		public String removed;
		public String[] channels;
		public String[] endpoints;
		public Object counterpoints;
		public String[] datastreamPaths;
		public String wavelengthCalibrationPath;
		
	}
	
	public static class ConnectionPoint {
		/** Table that the connection point is in, e.g. 'QSK' */
		public String table;
		
		/** Connection point is in, e.g. 'AEA21_A' */
		public String component;
		
		/** Endpoint connected to the previous component  */
		public String lEndpoint;
		
		/** Endpoint connected to the next component  */
		public String rEndpoint;
		
		/** Channel ID */		
		public String channel;		
	}
	
	/** Line of sight information */
	public static class LOSInfo {
		/** Component:Channel (no endpoint) for the component on the path that has a LOS definition */ 
		public String id;
		
		/** A position along the LOS, preferably not not necessarily a good place to 'start' (i.e. near the optic) */
		public double[] start;
		
		/** unit vector along LOS */
		public double[] uVec;
	}
	
	/** Structured information for a light path from the given connection point returned
	 * from getPathInfo() */
	public static class PathInfo {
		
		/** String version of full path 
		 *  e.g. "QS1/ReadThing[Panel]:13 = [Left]QS1/BlueThing[Right]:55 = ..."  */
		public String pathStr;
		
		/** List of connection points in the path */
		public List<ConnectionPoint> path;
		
		/** Datastreams of the first component in the path that has datastreams 
		 * (i.e. is an acqusition device) */
		public String[] streams;
		
		/** The component in the path that had datastream information */
		public String streamComponent;
		
		/** Line of sight information about the first object in the path with LOS info */ 
		public LOSInfo los;
		
		/** First time that the full connection path was valid. */
		public String tStart;
		
		/** Last time that the full connection path was valid. 
		 *  -1 means 'up until now' */
		public String tEnd;
	}	
	
	public static Component[] getComponents(String table, long time){
		
		String jsonText = makeJSONRequest("componentsList", table, time, "");
		
		JsonElement j = new JsonStreamParser(jsonText).next();
		
		Gson gson  = new Gson();
		return gson.fromJson(j, Component[].class);
		
	}

	public static Component getComponent(String table, String component, long time){
		
		String jsonText = makeJSONRequest("componentInfo", table, time, "&componentName="+component);
		
		JsonElement j = new JsonStreamParser(jsonText).next();
		
		Gson gson  = new Gson();
		return gson.fromJson(j, Component.class);
			
	}

	/** Get structured information for a light path from the given connection point */
	public static PathInfo getPathInfo(Database database, String table, String component, String endpoint, String channel, long time) {
		return getPathInfo(database, table, component, endpoint, channel, time, true, true);
	}
	
	/** Get structured information for a light path from the given connection point
	 * @param table :		Table that the connection point is in, e.g. 'QSK'
	 * @param component : 	Connection point is in, e.g. 'AEA21_A'
	 * @param endpoint : 	Endpoint from which to follow the connection
	 * @param channel : 	Channel
	 * @param time :		Time at which the connections are required
	 * @param getLifetime : Walk to database to find validity range and fill in tStart, tEnd (SLOW!) 
	 * @param suppressDefaultTable	: Drop table names for given table. 
	 * 									e.g. if table='QS1', 'QS1/Xyz' in path becomes just 'XYZ'
	 * @return
	 */	
	public static PathInfo getPathInfo(Database database, String table, String component, String endpoint, String channel, long time, 
			boolean getLifetime, boolean suppressDefaultTable) {
		return getPathInfo(database, table, component, endpoint, new String[]{ channel }, time, getLifetime, suppressDefaultTable)[0];
	}
	
	public static PathInfo[] getPathInfo(Database database, String table, String component, String endpoint, String[] channels, long time, 
											boolean getLifetime, boolean suppressDefaultTable) {
		
		String chanSpec = String.join(",", channels);
		try{
			chanSpec = URLEncoder.encode(chanSpec, "UTF8");
		} catch (UnsupportedEncodingException e) { throw new RuntimeException(e); }

		String params = "&componentName=" + component
						+ (endpoint != null ? "&endpoint" + endpoint : "")
						+ "&channels=" + chanSpec
						+ (getLifetime ? "" : "&noLifetime=True")								
						+ (suppressDefaultTable ? "&suppressDefaultTable=True" : "");
		
		String jsonText = makeJSONRequest("pathInfoMultichan", table, time, params);
			
		JsonElement j = new JsonStreamParser(jsonText).next();
		
		Gson gson  = new Gson();
		PathInfo[] pathInfos = gson.fromJson(j, PathInfo[].class);
		
		return pathInfos;
		
	}
	
	private static String makeJSONRequest(String request, String table, long time, String params){

		String tStr = NanoTime.toUTCString(time);
		//get rid of the T in the middle and the Z from the end so python understands the time
		tStr = tStr.replaceFirst("T", " ").replaceFirst("Z$", "");
		try{
			tStr = URLEncoder.encode(tStr, "UTF8");
		} catch (UnsupportedEncodingException e) { throw new RuntimeException(e); }

		String address = webapiAddress + "/"+request+".json"
							+ "?database=" + Defaults.getDatabase(table).toString()
							+ "&table=" + table
							+ "&time=" + tStr
							+ params;

		String jsonText = ArchiveDBFetcher.readUrl(address, -1);
		
		if(!jsonText.trim().startsWith("[") && !jsonText.trim().startsWith("{")){
			try {
				File f = File.createTempFile("LightPaths-error-", ".html");
				FileOutputStream os = new FileOutputStream(f);
				os.write(address.getBytes());
				os.write('\n');
				os.write(jsonText.getBytes());
				os.close();

				Matcher m = Pattern.compile(".*<title>(.*)</title>.*", Pattern.DOTALL).matcher(jsonText);
				String msg = m.matches() ? m.group(1) : "?";
				
				throw new RuntimeException("Non-JSON response  '"+msg+"' from '"+address+"'. Dumped to " + f);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}			
		}
		
		return jsonText;
	}

	public static void main(String[] args) {
		long t = NanoTime.convertDateTimeStringtoNanoseconds("2018-10-09T13:00:00.000000000Z");

		Component[] components = getComponents("QSK", t);
		for(Component c : components){
			System.out.println(c.name + ":\t" + c.datastreamPaths.length);
		}
		
		Component c = getComponent("QSK", "NIFS_He", t);
		System.out.println(c.name + ":");
		Mat.dumpArray(c.datastreamPaths);
		
		PathInfo[] infos = LightPaths.getPathInfo(Database.W7X_ARCHIVE, "QSK", "IXLS_Green", null, new String[]{ "21", "22", "23" }, t, true, true);
		
		for(PathInfo info : infos){
			System.out.println("Path: " + info.pathStr);
			System.out.println("From: " + info.tStart);
			System.out.println("To: " + info.tEnd);
		}
		
	}
		
}
