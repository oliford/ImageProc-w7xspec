package imageProc.w7xspec;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonStreamParser;

import net.jafama.FastMath;
import w7x.archive.ArchiveDBFetcher;

// Calls to Zeeman splitting and fine structure webservice
public class Zeeman {
	private static String webapiAddress = "http://" + Defaults.getWebservicesHost() + ":6055";
	
	public static class GaussianFitsResults {
		public String lineID;
		public double magB;
		public double theta;
		public double instfSigma;
		public double instfPower;

		public double l0Mass;		
		public double[] TiTrue;		
		public double[] TiFit;
		public double[] l0Fit;
		public double[] sigmaFit;
		public double[] intensityFit;		
	}
	
	public static void main(String[] args) {
		GaussianFitsResults o = getGaussianFits("carbon529line", 2.5, 0.0, 0.037, 2.5, false);
		
		System.out.println(o.l0Mass);
	}
	
	public static GaussianFitsResults getGaussianFits(String lineName, double B, double theta1, double instfSigma, double instfPower, boolean inhibitFS) {
		
		String params = "name=" + lineName +
						"&B=" + String.format("%.1f", B) +
						"&theta1=" + String.format("%d", FastMath.roundToInt(theta1 / 5) * 5) +
						"&angDep=" + (Double.isFinite(theta1) ? 1 : 0) +
						"&instfSigma=" + String.format("%.3f", instfSigma) +
						(inhibitFS ? "&inhibitFS=true" : "") +
						"&instfPower=" + String.format("%.1f", instfPower);
			
		String jsonText = makeJSONRequest("gaussianFit", params);
		
		JsonElement j = new JsonStreamParser(jsonText).next();
		
		Gson gson  = new Gson();
		return gson.fromJson(j, GaussianFitsResults.class);
			
	}
	
	private static String makeJSONRequest(String request, String params){
		String address = webapiAddress + "/"+request+"?" + params;
			
		String jsonText = ArchiveDBFetcher.readUrl(address, -1);
		
		if(!jsonText.trim().startsWith("[") && !jsonText.trim().startsWith("{")){
			try {
				File f = File.createTempFile("LightPaths-error-", ".html");
				FileOutputStream os = new FileOutputStream(f);
				os.write(address.getBytes());
				os.write('\n');
				os.write(jsonText.getBytes());
				os.close();

				Matcher m = Pattern.compile(".*<title>(.*)</title>.*", Pattern.DOTALL).matcher(jsonText);
				String msg = m.matches() ? m.group(1) : "?";
				
				throw new RuntimeException("Non-JSON response  '"+msg+"' from '"+address+"'. Dumped to " + f);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}			
		}
		
		return jsonText;
	}

}
