package imageProc.w7xspec;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import otherSupport.SettingsManager;
import signals.w7x.ArchiveDB.Database;

/** Annoying specifics of the W7X setup.
 * This is mostly for converting between different paths and specfiers
 *  e.g. "QSS" --> "QSS_DivertorSpectropscopy" --> "/raw/W7X/QSS_DivertorSpectroscopy/...."
 * 
 * Equivalent of python/W7XSpec/w7xspec/default.py
 * 
 *  Probably quite a lot of this should come from the LightPaths components DB
 */
public abstract class Defaults {
	
	/** Returns the default datastream path for known spectrometers */
	public static String getDatastreamPath(String system, String component){
	
	    if(system.equals("QSK_CXRS")){ 
	        if(component.equals("ILS_Red") 
	        		|| component.equals("ILS_Green")
	        		|| component.equals("PCOSpexM750")){
	        	//various spectrometers that were software binned
	            return "w7x/ArchiveDB/raw/W7X/" + system + "/" + component + "_Binned_DATASTREAM/0/Images";	        
	        }
	        
	    }else if(system.equals("QSK06_PassiveSpectroscopy")){ 
	        if(component.equals("PCOSpexM750") || component.equals("PCOSpexM750_200um")){
	        	//these are the same thing, sometimes slightly different name
	            return "w7x/ArchiveDB/raw/W7X/" + system + "/" + "PCOSpexM750" + "_Binned_DATASTREAM/0/Images";	        
	        }
            
		}else if(system.equals("QSZ_Zeff")){ 
		    if(component.equals("PI_CCD_07_1-QSS60OC094")){
		        //Zeff multichannel data still in test
		        return "w7x/Test/raw/W7X/QSZ_Zeff/PI_CCD_07_1-QSS60OC094_DATASTREAM/0/Images";
		    }
	    }
	    
	    //Plain default
	    return "w7x/ArchiveDB/raw/W7X/" + system + "/" + component + "_DATASTREAM/0/Images";
	}
    
	/** Default target names for fixed spectrometers */
	public static String getTarget(String system, String component){
	
	    if(system.equals("QSK_CXRS")){
	    	
	        if(component.equals("ILS_Green"))
	            return "C_VI_529";
	        else if(component.equals("ILS_Red"))
	            return "H_I_656";
	        else if(component.equals("ILS_Blue"))
	            return "He_II_468";
	        else if(component.equals("NIFS_H"))
	            return "H_I_656";
	        else if(component.equals("NIFS_He"))
	            return "He_II_468";
	        else if(component.equals("NIFS_C"))
	            return "C_VI_529";
	            	        
	
	    }else if(system.equals("QSK06_PassiveSpectroscopy")){
	        if(component.equals("PCOSpexM750") || component.equals("PCOSpexM750_200um") )
	            return "C_VI_529";
	        else if(component.equals("PassiveHalpha"))
	            return "H_I_656";
	        else if(component.equals("PassiveCarbon"))
	            return "C_VI_529";
	        
	    }else if(system.startsWith("QSZ") || system.startsWith("QSS")){ 
	        if(component.equals("PI_CCD_07_1-QSS60OC094"))
	            return "default"; //Nothing for now
	    }
	    
	    return null;

	}
	

	/**
	   Path to stored calibration data of a given type.
	
	   Parameters:
	     set : Calibration type, e.g. 'Wavelength' or 'Intensity'
	     target : Spectrometer configuration/settings target name
	 */
	public static String getCalibrationPath(String system, String component, String set, String target){
		String dbName = null;
		if(system.equals("QSZ_Zeff")){
			dbName = "Test";
		}else{ 
			dbName = "ArchiveDB";
		}
		
		if(system.equals("QSK06_PassiveSpectroscopy") && component.equals("PCOSpexM750_200um"))
			component = "PCOSpexM750";
	
		if(target.equalsIgnoreCase("good"))
			return dbName + "/raw/W7XAnalysis/" + system + "/Calibration_" + set + "_good_" + component + "_PARLOG";
		else
			return dbName + "/raw/W7XAnalysis/" + system + "/Calibration_" + set + "_" + component + "_" + target + "_PARLOG";
	}
	
	/**
    Fix channel names when the ROI names were not set properly
	 */
	public static String[] fixROINames(String system, String component, String[] chanNames) {
		Pattern pUnnamed = Pattern.compile("ch([0-9]*)");
	
	    if(system.startsWith("QSZ") || system.startsWith("QSS")) {
	        if(component.startsWith("PI_CCD_07_1")) {
	            for(int i=0; i < chanNames.length; i++) {
	            	if(chanNames[i] == null)
	            		continue;
	            	Matcher m = pUnnamed.matcher(chanNames[i]);               
	                if(m.matches()){
	                    chanNames[i] = component + ":" + m.group(1);
	                }
	            }
	        }
	    }
	    
	    if(system.equals("QSK_CXRS")){
	    	//During OP1.2b we used the head names "ILS:" and "NIFS:" instead of the
	    	//acquisition device component names "ILS_Green" etc
	    	if(component.startsWith("ILS_")){
	            for(int i=0; i < chanNames.length; i++) {
	            	chanNames[i] = chanNames[i].replaceAll("ILS:", component + ":");
	            }	    		
	    	}else if(component.startsWith("NIFS_")){
	            for(int i=0; i < chanNames.length; i++) {
	            	Matcher m = pUnnamed.matcher(chanNames[i]);               
	                if(m.matches()){
	                    chanNames[i] = component + ":" + String.format("%02d", (30 - i));
	                }else{
	                	chanNames[i] = chanNames[i].replaceAll("NIFS:", component + ":");
	                }
	            }	    		
	    	}
	    }
	    
	    //generic fix for things that didnt name their ROIs at all
	    for(int i=0; i < chanNames.length; i++) {
		    if(chanNames[i] != null && chanNames[i].matches("ch[0-9]*")) {
	    		chanNames[i] = chanNames[i].replace("ch", component+ ":");
			}
	    }
	    
	    return chanNames;
	}


	
	/** List of systems known to W7XSpec */
	public enum Table {
		QSK("QSK_CXRS"),
		QSK06("QSK06_PassiveSpectroscopy"),    
		QSS("QSS_DivertorSpectroscopy"),
		QSZ("QSZ_Zeff"),
		QSR07("QSR07_Scraper"), 
		CDX("CDX_NI_Spectroscopy"),
		QRI("QRI_CoherenceImaging"),
		//for tests:
		QS1("QS1_TestSpectroscopy"),
		QS2("QS2_TestSpectroscopy2");
		
		private final String systemName;		
		Table(String systemName){ this.systemName = systemName; }		
		public String systemName() { return this.systemName; }		

	}
	
	public static String getFullSystemName(String table){ 
		return getFullSystemName(Table.valueOf(table));
	}
		
	public static String getFullSystemName(Table table){ 
		return table.systemName();	
	}
	
	public static Table getTableFromSystemName(String systemName) {
		for(Table t : Table.values()) {
			if(systemName.equalsIgnoreCase(t.name()) || systemName.equalsIgnoreCase(t.systemName) )
				return t;
		}
		return null;
	}
	
	public static Database getDatabase(String table){
		return getDatabase(Table.valueOf(table));
	}
	
	public static Database getDatabase(Table table){
		switch(table){
			case QSK: return Database.W7X_ARCHIVE;
			case QSK06: return Database.W7X_ARCHIVE;
			case QSS: return  Database.W7X_TEST;
			case QSZ: return  Database.W7X_TEST;
			case QSR07: return Database.W7X_ARCHIVE;
			case CDX: return Database.W7X_ARCHIVE;
			case QRI: return  Database.W7X_ARCHIVE; 
			case QS1: return  Database.W7X_TEST;
			case QS2: return  Database.W7X_TEST; 
			default: return null;
		}
	}

	public static String getWebservicesHost() {
		return SettingsManager.defaultGlobal().getProperty("w7xspec.services.host", "sv-coda-wsvc-28.ipp-hgw.mpg.de");		
	}

	/** Try to determine a table name from a stream path.
	 * (This doesn't look the streams up in the database. It just guessed based on the known convention) */
	public static String getTableFromDatastream(String streamPath) {
		
		if(streamPath == null)
			throw new RuntimeException("table not set and no datastream path in metadata with which to guess it");
		
		Pattern p = Pattern.compile(".*/(Q[A-Z0-9]*)[-_/].*");
		Matcher m = p.matcher(streamPath);
		if(m.matches()) {
			return m.group(1);
			
		}else {
			
			Pattern pGERIStreamed = Pattern.compile(".*/ControlStation.(71[0-9][0-9][0-9])/.*");
			Matcher m2 = pGERIStreamed.matcher(streamPath);
			if(m2.matches()) {
				int stationID = Integer.parseInt(m2.group(1)); 
				switch((int)(stationID / 100)){
				case 713:
					return "QSK";
				case 718:
					return "QSK06";
				default:
					throw new RuntimeException("GERI streamed data from unknown group (diagnostic) " + m2.group(1));
				}
				
			}else {
				throw new RuntimeException("datastream path didn't contain something like '/QXX_' (or ControlStation.713xx)");
			}
		}
	}
	
	/** Try to determine a spectrometer name from a stream path.
	 * (This doesn't look the streams up in the database. It just guessed based on the known convention) */
	public static String getSpectrometerFromDatastream(String streamPath) {
		//Instead just guess from stream name
		Pattern p = Pattern.compile(".*/Q[^/]*/([^_]*).*");
		Matcher m = p.matcher(streamPath);
		if(m.matches()) {
			return m.group(1);
		}else {					
			Pattern p2 = Pattern.compile(".*/ControlStation.713[0-9][0-9]/(.*)-1_DATASTREAM.*");
			Matcher m2 = p2.matcher(streamPath);
			if(m2.matches()) {
				return m2.group(1);						

			}else {
				throw new RuntimeException("datastream path didn't contain something like '/Q.../xxxxx_' or ControlStation.713../xxxx_");
			}
		}
	}
}
