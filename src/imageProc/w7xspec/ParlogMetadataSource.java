package imageProc.w7xspec;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;
import w7x.archive.ArchiveDBFetcher;

/** Callback for the CameraInfoService to get it's metadata from the parlog.
 * This is mostly dealing with JSON. */ 
public class ParlogMetadataSource implements MetaDataGetter {
	public JsonElement parlogJSON;
	public ArchiveDBFetcher adb;
	public Gson gson;
	
	HashMap<String, Object> overrides = new HashMap<String, Object>();
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String name, Class<T> desiredType) {
		name = name.replaceAll("^\\s*/*", ""); //strip leading slashes and spaces
					
		Object override = overrides.get(name);
		if(override != null) {
			System.out.println("DEBUG: Overriding/inserting metadata: "+name+" = "+override);
			return (T) override;
		}
		
		JsonElement elem = ArchiveDBFetcher.extractJsonEntry(parlogJSON, "parms/" + name);
		
		if(elem.isJsonObject()) {
			JsonObject jo = elem.getAsJsonObject(); 
			//if(elem.getAsJsonObject().has("[0]"))
			//looks like array stupidity
			
			if(!desiredType.isArray())
				throw new RuntimeException("Got a JSON object, but we're not wanting an array");
			
			int max = 0;
			for(String key : jo.keySet()) {
				String idxStr = key.substring(1, key.length()-1);
				int i = Integer.parseInt(idxStr);
				if(i > max)
					max = i;
			}
			Object array = Array.newInstance(desiredType.getComponentType(), max+1);
			
			for(Entry<String, JsonElement> entry2 : jo.entrySet()) {
				String key = entry2.getKey();
				String idxStr = key.substring(1, key.length()-1);
				int i = Integer.parseInt(idxStr);
				Object o = gson.fromJson(entry2.getValue(), desiredType.getComponentType());
				Array.set(array, i, o);
			}				
			return (T) array;
		}else if(elem.isJsonArray() && !desiredType.isArray() && elem.getAsJsonArray().size() == 1) {
			//oh ffs, someone wrapped our primitive in a 1 element array
			elem = elem.getAsJsonArray().get(0);
			return gson.fromJson(elem, desiredType);
			
		}else {
			//otherwise try direct conversion
			return gson.fromJson(elem, desiredType);
		}
	}

	public void addOverride(String name, Object value) {
		overrides.put(name, value);			
	}
}