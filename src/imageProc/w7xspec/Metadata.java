package imageProc.w7xspec;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBDesc.Type;
import descriptors.w7x.ArchiveDBJSONInfoDesc.ExtraJsonRequestArg;
import fusionDefs.sensorInfo.SensorInfo;
import fusionDefs.sensorInfo.SensorInfoService;
import w7x.archive.ArchiveDBFetcher;

/** Relevant metadata from the parlog JSONs. 
 * 
 * This will partially parallel python.w7xspec.Metadata
 * 
 * @author oliford
 *
 */
public class Metadata {
	

	/** This should probably be in ImageProc-w7xspec */
	public static class ROIsArrays {
		public String[] names;
		public int[] x;
		public int[] y;
		public int[] width;
		public int[] height;
		public int[] x_binning;
		public int[] y_binning;
	}

	public static ROIsArrays getROIs(JsonElement parlogJSON) {		
		try {
			JsonElement json = findJsonElement("Rois_arrays", parlogJSON);
			
			Gson gson = new Gson();
			ROIsArrays rois = gson.fromJson(json, ROIsArrays.class);
			
			return rois;
			
		}catch(RuntimeException err) {
			throw new RuntimeException("Couldn't get a valid 'Rois_arrays' metadata item in parlog. Can't figure out the channel names", err);
		}
	}
	
	public static JsonElement findJsonElement(String name, JsonElement j) {
		if(j.isJsonArray()) {
			//array has no names, so descend
			for(JsonElement j2 : j.getAsJsonArray()) {
				JsonElement ret = findJsonElement(name, j2);
				if(ret != null)
					return ret;
			}
			return null;
			
		}else if(j.isJsonObject()) { 
			JsonObject jo = j.getAsJsonObject();
			for(String key : jo.keySet()) {
				JsonElement val = jo.get(key);
				
				//look for the given name
				if(name.equals(key))
					return val;
				
				//otherwise descend
				JsonElement ret = findJsonElement(name, val);
				if(ret != null)
					return ret;
			}
			return null;
		}else { //other type and we didnt see the name
			return null;
		}
	}

	public static SensorInfo getSensorInfo(ArchiveDBDesc parlogDesc, long nanotime) {

		//json
		ParlogMetadataSource parlogMetadata = new ParlogMetadataSource();
		parlogMetadata.adb = ArchiveDBFetcher.defaultInstance().quiet();
		parlogMetadata.parlogJSON = parlogMetadata.adb.getParlogJson(parlogDesc, ExtraJsonRequestArg.MAP_TO_ARRAY);			
		parlogMetadata.gson = new Gson();
		
		return SensorInfoService.decipher(parlogMetadata); 
		
	}
	
	/** Some metadata fields can be added to the notes, for the user to specify overrides and corrections
	 * for specific shots, e.g. the target, wavelength offset etc 
	 * @param desc Description of datastream or parlog of acquisition
	 * @param name Field
	 */
	public static String getMetadataFromNotes(ArchiveDBDesc desc, String name) {
    	//try to pick up target from notes
    	
		String signalName = desc.getSignalName();		
		desc.setSignalName(signalName + "_Notes");
		desc.setType(Type.PARLOG);
		desc.setChannelName(null);
		desc.setChannelNumber(-1);
		//desc.setNanosRange(time, time);
		//System.err.println(desc.getWebAPIRequestURI());
		desc.setVersion(0);
		
		ArchiveDBFetcher adb = new ArchiveDBFetcher();				
		adb.enableMutableCache(false);
		int latest = adb.getHighestVersionNumber(desc);
		adb.enableMutableCache(true);
		desc.setVersion(latest);
		
		try {
			String notes = adb.getParLogEntry(desc, "parms/notes", String.class);		
			Pattern p = Pattern.compile(".*"+name+"=([^\\s]*).*", Pattern.DOTALL);
			Matcher m = p.matcher(notes);
			if(m.matches()){
				return m.group(1);
			}
		}catch(RuntimeException err){
			if(!err.getMessage().contains("EmptySignal")) {
				Logger.getLogger(Metadata.class.getName()).log(Level.FINE, "Couldnt get notes while triyng to find target", err);
			}
		} 
		return null;
    }
}
